module Tests

open System
open Xunit
open MotherShip
open FsCheck.Xunit


let toDirection (dir:string) =
    match dir.ToLower() with
    |"north" -> North
    |"south" -> South
    |"east" -> East
    |"west" -> West
    |_ -> Unknown


[<Property>]
let ``Rover position is set correctly`` (x: UInt32, y:UInt32, direction:Direction) =
    let coordinates:Coordinate = {X = x ; Y = y}
    let direction : Direction = East
    let rover = {Coordinates = coordinates ; Direction = direction};
    Assert.Equal(coordinates.X, rover.Coordinates.X)
    Assert.Equal(coordinates.Y, rover.Coordinates.Y)
    Assert.Equal(direction, rover.Direction)

[<Theory>]
[<InlineData(1,1,"North","L", 1,1,"West")>]
[<InlineData(1,1,"North","R", 1,1,"East")>]
[<InlineData(1,1,"North","M", 1,2,"North")>]
[<InlineData(1,1,"North","LM", 0,1,"West")>]
[<InlineData(1,1,"North","RM", 2,1,"East")>]
[<InlineData(1,1,"North","LLM", 1,0,"South")>]
[<InlineData(1,1,"North","RMM", 3,1,"East")>]
let ``Rover postion is changed after move instruction``(intialX,intialY,direction,instruction,finalX,finalY, finalDirection) =

    let initialCoordinates:Coordinate = {X = intialX ; Y = intialY}
    let initialDirection : Direction = toDirection direction
    let rover = {Coordinates = initialCoordinates ; Direction = initialDirection};
    
    let roverAfterMove =  move rover instruction
    Assert.Equal((toDirection  finalDirection), roverAfterMove.Direction)
    Assert.Equal(finalX, roverAfterMove.Coordinates.X)
    Assert.Equal(finalY, roverAfterMove.Coordinates.Y)
    
[<Theory>]
[<InlineData("10 10", "1 2 N", "MM", "3 5 E", "MM", "1 4 N","5 5 E")>]
[<InlineData("5 5", "1 2 N", "LMLMLMLMM", "3 3 E", "MMRMMRMRRM", "1 3 N","5 1 E")>]
let ``The final position of the rovers is set correctly``(topUpper, firstRoverCoordinatesAndDirection,firstRoverCommand,secondRoverCoordinatesAndDirection,secondRoverCommad,expectedFirstRoverPostionAndDirection, expectedSecondRoverPostionAndDirection ) =
    
    let (actualFirstRoverPositionAndDirection, actualSecondRoverPositionAndDirection) = 
        executeCommand topUpper firstRoverCoordinatesAndDirection firstRoverCommand secondRoverCoordinatesAndDirection secondRoverCommad

    Assert.Equal(expectedFirstRoverPostionAndDirection, actualFirstRoverPositionAndDirection )
    Assert.Equal(expectedSecondRoverPostionAndDirection, actualSecondRoverPositionAndDirection )
