using MarsRover;
using NUnit.Framework;
using System;
using System.Linq;

namespace Tests
{
    public class Tests
    {

        [TestCase(10, 10, 2, 2, 3, FacingDirection.N ,"LLM",2,2,FacingDirection.S, 4,6, FacingDirection.E, "RRM", 3,6,FacingDirection.W)]
        [TestCase(5, 5, 2, 1, 2, FacingDirection.N, "LMLMLMLMM", 1, 3, FacingDirection.N, 3, 3, FacingDirection.E, "MMRMMRMRRM", 5, 1, FacingDirection.E)]
        public void GridSizeIsSetCorrectly(int x, int y, int roverCount, 
            int x1, int y1, FacingDirection facingDirection1, 
            string firstRoverMoveInstructions, int fx1, int fy1, FacingDirection finalFacingDirection1,
            int x2, int y2, FacingDirection facingDirection2,
            string secondRoverMoveInstructions, int fx2, int fy2, FacingDirection finalFacingDirection2
            )
        {
           
            
            Grid grid = new Grid(roverCount);

            grid.Instruction($"{x} {y}", $"{x1} {y1} {(int)facingDirection1}",firstRoverMoveInstructions, $"{x2} {y2} {(int)facingDirection2}", secondRoverMoveInstructions);

            Assert.IsTrue(grid.XMax == x);
            Assert.IsTrue(grid.YMAx == y);
            Assert.IsTrue(grid.Rovers.Count() == roverCount);

            Rover firstRover = grid.Rovers.First();
                                    
            Assert.IsTrue(firstRover.XPosition == fx1);
            Assert.IsTrue(firstRover.YPosition == fy1);
            Assert.IsTrue(firstRover.FacingDirection == finalFacingDirection1);

            Rover secondRover = grid.Rovers.Skip(1).First();
                        
            Assert.IsTrue(secondRover.XPosition == fx2);
            Assert.IsTrue(secondRover.YPosition == fy2);
            Assert.IsTrue(secondRover.FacingDirection == finalFacingDirection2);

        }


        [Test]
        public void IntialPostionOfRoverIsSetCorrectly()
        {
            int x = 2, y = 4;
            FacingDirection facingDirection = FacingDirection.E;
            Rover r = new Rover();
            r.SetRoverPosition(x, y, facingDirection);
            Assert.IsTrue(r.XPosition == x);
            Assert.IsTrue(r.YPosition == y);
            Assert.IsTrue(r.FacingDirection == facingDirection);
        }

        [Test]
        public void RoverPostionIsSetCorrectly()
        {
            int x = 5, y = 7;
            FacingDirection direction = FacingDirection.N;
            Rover rover = new Rover(x, y, direction);
            Assert.IsTrue(rover.XPosition == x);
            Assert.IsTrue(rover.YPosition == y);
            Assert.IsTrue(rover.FacingDirection == direction);
        }


        [TestCase(5, 5, FacingDirection.S, "L", 5, 5, FacingDirection.E)]
        [TestCase(5, 5, FacingDirection.E, "L", 5, 5, FacingDirection.N)]
        [TestCase(5, 5, FacingDirection.N, "L", 5, 5, FacingDirection.W)]
        [TestCase(5, 5, FacingDirection.W, "L", 5, 5, FacingDirection.S)]
        [TestCase(5, 5, FacingDirection.S, "R", 5, 5, FacingDirection.W)]
        [TestCase(5, 5, FacingDirection.W, "R", 5, 5, FacingDirection.N)]
        [TestCase(5, 5, FacingDirection.N, "R", 5, 5, FacingDirection.E)]
        [TestCase(5, 5, FacingDirection.E, "R", 5, 5, FacingDirection.S)]
        [TestCase(5, 5, FacingDirection.E, "M", 6, 5, FacingDirection.E)]
        [TestCase(5, 5, FacingDirection.S, "M", 5, 4, FacingDirection.S)]
        [TestCase(5, 5, FacingDirection.W, "M", 4, 5, FacingDirection.W)]
        [TestCase(5, 5, FacingDirection.N, "M", 5, 6, FacingDirection.N)]
        [TestCase(5, 5, FacingDirection.E, "RM", 5, 4, FacingDirection.S)]
        [TestCase(5, 5, FacingDirection.N, "MM", 5, 7, FacingDirection.N)]
        [TestCase(5, 5, FacingDirection.N, "LR", 5, 5, FacingDirection.N)]
        public void RoverIsFacingWestWhenItMovesFrontItsXCoordinatesChanges
            (int sx, int sy, FacingDirection sDir, string moves,
            int ex, int ey, FacingDirection eDir)
        {

            Rover rover = new Rover(sx, sy, sDir);

            rover.Move(moves);
            Assert.IsTrue(rover.XPosition == ex);
            Assert.IsTrue(rover.YPosition == ey);
            Assert.IsTrue(rover.FacingDirection == eDir);
        }
    }
}