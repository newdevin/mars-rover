﻿module MotherShip

open System

type Direction = 
    East
    |West
    |North
    |South
    | Unknown
type Coordinate = {X:UInt32; Y:UInt32}
type Rover = {Coordinates:Coordinate ; Direction:Direction}
type Grid = {Coordinates:Coordinate}
let move (rover:Rover) (instruction:string): Rover=
    let turnLeft rover = 
        match rover.Direction with
        | East -> {rover with Direction = North}
        | North -> {rover with Direction = West}
        | West -> {rover with Direction = South}
        | South -> {rover with Direction = East}
        | Unknown -> rover

    let turnRight rover = 
        match rover.Direction with
        | East -> {rover with Direction = South}
        | North -> {rover with Direction = East}
        | West -> {rover with Direction = North}
        | South -> {rover with Direction = West}
        | Unknown -> rover

    let moveForward rover = 
        match rover.Direction with
        | North -> {rover with Coordinates = {X = rover.Coordinates.X ; Y = rover.Coordinates.Y + 1ul}}
        | East ->  {rover with Coordinates = {X = rover.Coordinates.X  + 1ul; Y = rover.Coordinates.Y}}
        | South ->  {rover with Coordinates = {X = rover.Coordinates.X; Y = (rover.Coordinates.Y - 1ul)}}
        | West ->  {rover with Coordinates = {X = rover.Coordinates.X  - 1ul; Y = rover.Coordinates.Y}}
        | Unknown -> rover

    let rec moverec rover command = 
        match command with
        | 'L'::tail -> let nrover = turnLeft rover 
                       moverec nrover tail
        | 'R'::tail -> let nrover = turnRight rover
                       moverec nrover tail
        | 'M'::tail -> let nrover = moveForward rover
                       moverec nrover tail
        | _ -> rover

    instruction |> Seq.toList |> moverec rover

let toDirection ch = 
    match ch with
    |"E" -> East
    |"W" -> West
    |"N" -> North
    |"S" -> South
    | _ -> Unknown
    
let directionToString direction = 
    match direction with
    | East -> "E"
    | West -> "W"
    | North -> "N"
    | South -> "S"
    | Unknown -> ""

let parseCoordinatesAndDirection (s :string)=
    let s1 = s.Split(" ", StringSplitOptions.RemoveEmptyEntries)
    let x1 = match UInt32.TryParse(s1.[0]) with
             | true, x -> Some x
             | false, _ -> None

    let y1 = match UInt32.TryParse(s1.[1]) with
             | true, x -> Some x
             | false, _ -> None
    let d1 = toDirection s1.[2]
    if x1.IsSome && y1.IsSome then
        Some ({X = x1.Value; Y = y1.Value},d1)
    else
        None

let executeCommand (topRight:string) (firstRoverPositionAndDirection:string) (firstRoverInstruction:string) (secondRoverPositionAndDirection:string) (secondRoverInstruction:string) = 
    
    let s = topRight.Split(" ", StringSplitOptions.RemoveEmptyEntries)
    let x = match UInt32.TryParse(s.[0]) with
            | true, x -> Some x
            | false, _ -> None
    let y = match UInt32.TryParse(s.[1]) with
            | true, x -> Some x
            | false, _ -> None
    if x.IsNone || y.IsNone then
        ("","")
    else
        let rover1Pos = match parseCoordinatesAndDirection firstRoverPositionAndDirection with
                        | Some (p,d) -> 
                            let rover1 = {Coordinates = {X = p.X ; Y = p.Y} ; Direction = d}
                            let nRover1 = move rover1 firstRoverInstruction
                            sprintf "%i %i %s" nRover1.Coordinates.X nRover1.Coordinates.Y (directionToString nRover1.Direction)
                        | None -> ""
        let rover2Pos = match parseCoordinatesAndDirection secondRoverPositionAndDirection with
                        | Some (p,d) -> 
                            let rover1 = {Coordinates = {X = p.X ; Y = p.Y} ; Direction = d}
                            let nRover1 = move rover1 secondRoverInstruction
                            sprintf "%i %i %s" nRover1.Coordinates.X nRover1.Coordinates.Y (directionToString nRover1.Direction)
                        | None -> ""
        (rover1Pos, rover2Pos)
        
        
