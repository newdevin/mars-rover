﻿using System.Collections.Generic;

namespace MarsRover
{
    public class Grid
    {
        public int XMax { get; private set; }
        public int YMAx { get; private set; }

        public IEnumerable<Rover> Rovers => rovers;

        List<Rover> rovers = new List<Rover>();


        public Grid(int numberOfRovers)
        {
            for (int i = 0; i < numberOfRovers; i++)
            {
                rovers.Add(new Rover());
            }
        }

        public void Instruction(string position, string firstRoverPosition, string firstRoverMoveInstructions, 
            string secondRoverPosition, string secondRoverMoveInstructions)
        {
            string[] sizeArray = position.Split(" ");
            XMax = int.Parse(sizeArray[0]);
            YMAx = int.Parse(sizeArray[1]);

            string[] firstRoverData = firstRoverPosition.Split(" ");
            var x1 = int.Parse(firstRoverData[0]);
            var y1 = int.Parse(firstRoverData[1]);
            FacingDirection facingDirection1 = (FacingDirection)int.Parse(firstRoverData[2]);

            var firstRover = rovers[0];
            firstRover.SetRoverPosition(x1, y1, facingDirection1);
            firstRover.Move(firstRoverMoveInstructions);

            string[] secondRoverData = secondRoverPosition.Split(" ");
            var x2 = int.Parse(secondRoverData[0]);
            var y2 = int.Parse(secondRoverData[1]);
            FacingDirection facingDirection2 = (FacingDirection)int.Parse(secondRoverData[2]);

            var secondRover = rovers[1];
            secondRover.SetRoverPosition(x2, y2, facingDirection2);
            secondRover.Move(secondRoverMoveInstructions);
                        


        }


    }
}
