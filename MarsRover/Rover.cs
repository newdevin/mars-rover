﻿namespace MarsRover
{
    public class Rover
    {
        public int YPosition { get; private set; }
        public int XPosition { get; private set; }
        public FacingDirection FacingDirection { get; private set; }


        public Rover(int xPosition, int yPosition, FacingDirection facingDirection)
        {
            FacingDirection = facingDirection;
            XPosition = xPosition;
            YPosition = yPosition;
        }

        public Rover()
        {

        }

        public void Move(string move)
        {
            foreach (char s in move)
            {
                switch (s)
                {
                    case 'L':
                        TurnLeft();
                        break;
                    case 'R':
                        TurnRight();
                        break;
                    case 'M':
                        Move();
                        break;
                }
            }
        }

        private void Move()
        {
            if (FacingDirection == FacingDirection.N)
                YPosition = YPosition + 1;
            else if (FacingDirection == FacingDirection.S)
                YPosition = YPosition - 1;
            else if (FacingDirection == FacingDirection.E)
                XPosition = XPosition + 1;
            else if (FacingDirection == FacingDirection.W)
                XPosition = XPosition - 1;
        }

        private void TurnRight()
        {
            if (FacingDirection == FacingDirection.S)
                FacingDirection = FacingDirection.W;
            else if (FacingDirection == FacingDirection.W)
                FacingDirection = FacingDirection.N;
            else if (FacingDirection == FacingDirection.N)
                FacingDirection = FacingDirection.E;
            else if (FacingDirection == FacingDirection.E)
                FacingDirection = FacingDirection.S;
        }

        private void TurnLeft()
        {
            if (FacingDirection == FacingDirection.S)
                FacingDirection = FacingDirection.E;
            else if (FacingDirection == FacingDirection.E)
                FacingDirection = FacingDirection.N;
            else if (FacingDirection == FacingDirection.N)
                FacingDirection = FacingDirection.W;
            else if (FacingDirection == FacingDirection.W)
                FacingDirection = FacingDirection.S;
        }

        public void SetRoverPosition(int x, int y, FacingDirection facingDirection)
        {
            XPosition = x;
            YPosition = y;
            FacingDirection = facingDirection;
        }
    }

    public enum FacingDirection
    {
        N = 1,
        S = 2,
        E = 3,
        W = 4
    }

}
